// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"
import 'bootstrap'
import "@fortawesome/fontawesome-free/js/all"
import Masonry from 'masonry-layout';

Rails.start()
Turbolinks.start()
ActiveStorage.start()

$(document).on('turbolinks:load', function () {
    if ($('.posts-layout').length > 0) {
        new Masonry('.posts-layout', {
            percentPosition: true
        });
    }
    $('.delete-btn').click(function () {
        let id = $(this).data('id');
        $('#delete-confirm-modal').find('input[name=id]').val(id);
    });

})

