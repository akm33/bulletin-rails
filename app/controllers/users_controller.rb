class UsersController < ApplicationController
  before_action :set_current_user, only: %i[edit update edit_password update_password destroy]

  def new
    @user = User.new
  end

  def create
    user_params = params.require(:user).permit(:name, :email, :password, :username, :gender, :address)
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = 'You have been registered successfully.'
      redirect_to root_path
    else
      render 'new'
    end
  end

  def profile; end

  def edit; end

  def update
    if @user.update(user_params)
      flash[:notice] = 'Your Profile was updated successfully.'
      redirect_to profile_path
    else
      render 'edit'
    end
  end

  def edit_password; end

  def update_password
    password_params = params.require(:user).permit(:current_password, :password)
    if UsersService.update_password(@user, password_params).errors.empty?
      flash[:notice] = 'Password Updated.'
      redirect_to profile_path
    else
      render 'edit_password'
    end
  end

  def destroy
    @user.destroy
    flash[:alert] = 'Your Account has been deleted.'
    session[:user_id] = nil
    redirect_to root_path
  end

  private

  def set_current_user
    @user = current_user
  end

  def user_params
    params.require(:user).permit(:name, :email, :username, :gender, :address)
  end

end
