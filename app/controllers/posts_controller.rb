class PostsController < ApplicationController
  before_action :fetch_post, only: %i[show edit update destroy]
  before_action :check_public_post, only: %i[show edit]
  before_action :require_same_user, only: %i[edit update destroy]

  def index
    author_id = logged_in? ? current_user.id : nil
    @posts = PostsService.get_posts(author_id, params[:page], 15)
  end

  def show; end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user
    if @post.save
      flash[:notice] = 'Post was created successfully.'
      redirect_to @post
    else
      render 'new'
    end
  end

  def edit; end

  def update
    if @post.update(post_params)
      flash[:notice] = 'Post was updated successfully.'
      redirect_to @post
    else
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    flash[:notice] = "Post with [id = #{@post.id}] was deleted"
    redirect_to root_path
  end

  private

  def fetch_post
    @post = PostsService.get_post(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :message, :is_public)
  end

  def check_public_post
    return if @post.is_public || @post.user == current_user

    flash[:alert] = 'You are not allowed to view this post'
    redirect_to root_path
  end

  def require_same_user
    return if current_user == @post.user

    flash[:alert] = 'You can only edit or delete your own article'
    redirect_to @post
  end

end
