class AuthController < ApplicationController

  def show_login
    @user_auth = User.new
    render 'login'
  end

  def login
    @user_auth = UsersService.authenticate(params.permit(:email, :password))
    if @user_auth.errors.size.zero?
      session[:user_id] = @user_auth.id
      redirect_to root_path
    else
      render 'login'
    end
  end

  def logout
    session[:user_id] = nil
    redirect_to root_path
  end

end
