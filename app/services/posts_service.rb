class PostsService
  class << self
    def get_posts(author_id, page, per_page = 10)
      PostsRepository.get_posts(author_id, page, per_page)
    end

    def get_post(id)
      PostsRepository.get_post(id)
    end
  end
end
