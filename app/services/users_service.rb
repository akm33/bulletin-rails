class UsersService
  class << self

    def authenticate(params)
      auth_fail_message = 'Email or password is wrong, please try again.'
      user = find_user_by_email(params[:email])
      if user
        user.errors.add(:login, auth_fail_message) unless user.authenticate(params[:password])
      else
        user = User.new(params)
        user.errors.add(:login, auth_fail_message)
      end
      user
    end

    def find_user_by_email(email)
      return nil if email.blank?

      UsersRepository.get_user_by_email(email.strip.downcase)
    end

    def update_password(user, params)
      user.errors.add(:password, 'Enter New Password') if params[:password].blank?
      user.errors.add(:current_password, 'Enter Current Password') if params[:current_password].blank?
      user.update(params) if user.errors.empty?
      user
    end

  end
end