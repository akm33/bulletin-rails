class Post < ApplicationRecord
  belongs_to :user, foreign_key: 'author_id'
  validates :title, presence: true, length: { maximum: 250 }
  validates :message, presence: true, length: { maximum: 2000 }
end
