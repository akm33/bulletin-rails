class User < ApplicationRecord
  has_secure_password

  attr_accessor :current_password

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :name, presence: true
  validates :email,
            presence: true,
            uniqueness: { case_sensitive: false },
            length: { maximum: 50 },
            format: { with: VALID_EMAIL_REGEX }
  validates :password, presence: true, on: :create
  validates :username, presence: true, length: { minimum: 4, maximum: 20 }
  validates :address, allow_blank: true, length: { maximum: 100 }
  validates :gender, presence: true
  validates :address, presence: true, length: { maximum: 100 }
  validate :current_password_is_correct, unless: proc { |user| user.current_password.nil? }

  def password_validation
    new_record? || password_digest_changed?
  end

  def current_password_is_correct
    errors.add(:current_password, 'is incorrect.') if User.find(id).authenticate(current_password) == false
  end
end
