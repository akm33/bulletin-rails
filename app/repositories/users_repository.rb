class UsersRepository
  class << self
    def get_user_by_email(email)
      User.find_by(email: email)
    end
  end
end
