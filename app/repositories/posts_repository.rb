class PostsRepository
  class << self
    def get_posts(author_id, page, per_page = 10)
      query = Post.order(updated_at: :desc).where(is_public: true)
      query = query.or(Post.where(author_id: author_id)) if author_id
      query.paginate(page: page, per_page: per_page)
    end

    def get_post(id)
      Post.find(id)
    end
  end
end
