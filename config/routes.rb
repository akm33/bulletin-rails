Rails.application.routes.draw do
  root 'posts#index'
  resources :posts, except: %i[index delete]
  delete 'post/delete', to: 'posts#destroy'

  get 'login', to: 'auth#show_login'
  post 'login', to: 'auth#login'
  post 'logout', to: 'auth#logout'

  get 'register', to: 'users#new'
  post 'register', to: 'users#create'

  get 'profile', to: 'users#profile'
  get 'profile/edit', to: 'users#edit', as: 'profile_edit'
  patch 'profile/update', to: 'users#update', as: 'profile_update'
  delete 'profile/delete', to: 'users#destroy'

  get 'password/edit', to: 'users#edit_password', as: 'password_edit'
  patch 'password/update', to: 'users#update_password', as: 'password_update'
end
