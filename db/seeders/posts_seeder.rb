require 'faker'

def post_seeding(total = 20)
  puts 'Seeding Posts...'
  users = User.pluck(:id)
  (1..total).each do
    user = users.sample
    Post.create!(
      title: Faker::Lorem.sentence(word_count: 6, supplemental: true, random_words_to_add: 10),
      message: Faker::Lorem.paragraph(sentence_count: 40, supplemental: true, random_sentences_to_add: 40),
      is_public: Faker::Boolean.boolean(true_ratio: 0.8),
      author_id: user,
      created_at: Faker::Date.between(from: 10.days.ago, to: Date.today),
      updated_at: Faker::Date.between(from: 10.days.ago, to: Date.today)
    )
  end
end

