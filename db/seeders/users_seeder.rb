require 'faker'

def user_seeding(total = 20)
  puts 'Seeding User...'
  User.create!(
    username: 'admin',
    name: 'Admin',
    email: 'admin@mail.com',
    password: '12345678',
    address: 'Yangon',
    gender: 'm',
    is_admin: true
  )

  (1..total).each do
    first_name = Faker::Name.first_name_neutral
    last_name = Faker::Name.last_name
    User.create!(
      username: first_name.downcase + Random.rand(999).to_s,
      name: "#{first_name} #{last_name}",
      email: Faker::Internet.email(name: "#{first_name} #{last_name}"),
      password: '12345678',
      address: Faker::Address.city,
      gender: Faker::Boolean.boolean ? 'm' : 'f',
      is_admin: Faker::Boolean.boolean(true_ratio: 0.3)
    )
  end

end
