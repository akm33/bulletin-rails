class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :username, limit: 20
      t.string :name
      t.string :email, unique: true
      t.string :password_digest
      t.string :address
      t.string :gender, limit: 1
      t.boolean :is_admin, default: false
      t.timestamps
    end
  end
end
