class CreatePosts < ActiveRecord::Migration[6.1]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :message
      t.boolean :is_public, default: true
      t.references :author, index: true, foreign_key: {to_table: :users, on_delete: :cascade}
      t.timestamps
    end
  end
end
